#ifdef _WIN32
	#include "Python.h"
#else
	#include <Python/Python.h>
	//#include <libgen.h>
#endif
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

#define true 1
#define false 1

#define DB_FILENAME "tzposdb.data"

typedef int bool;

struct Position {
	double lat;
	double lon;
	char name[30];
};

#ifdef _WIN32
size_t getline(char ** lineptr, size_t *n, FILE * file) {
	return (size_t)1;
}
#endif

unsigned int dbsize;
struct Position** db;

char* timezone_at(const struct Position pos)
{
	double dx, dy, d;
	double minval = 9999.0f;
	struct Position *cpos;
	struct Position *ppos;
	int i;
	
	for(i = 0; i < dbsize; i++) {
		cpos = db[i];
		dx = fabs(cpos->lon - pos.lon);
		dy = fabs(cpos->lat - pos.lat);
		d = sqrt((dx * dx) + (dy * dy));

		if (d < minval)
		{
			ppos = cpos;
			minval = d;
		}
	}

	return ppos->name;
}

static PyObject *
tzpos_timezone_at(PyObject *self, PyObject *args)
{
	double lat;
	double lon;
	struct Position pos;
	char * tz;

	if (!PyArg_ParseTuple(args, "dd", &lat, &lon))
	{
		return NULL;
	}
	
	pos.lat = lat;
	pos.lon = lon;

	tz = timezone_at(pos);

	return Py_BuildValue("s", tz);
}

static PyObject *
tzposc_load_default_db(PyObject *self, PyObject *args)
{
	PyObject *pydb;
	PyObject *pos;
	PyObject *olat, *olon;
	struct Position *position;
	size_t szpos;
	int i;

	if (!PyArg_ParseTuple(args, "O", &pydb))
		return NULL;

	szpos = sizeof(struct Position);
	dbsize = PyList_Size(pydb);
	db = malloc(szpos * dbsize);
	

	for (i = 0; i < dbsize; i++) {
		position = (struct Position*)malloc(szpos);
		pos = PyList_GetItem(pydb, i);
		olat = PyList_GetItem(pos, 0);
		olon = PyList_GetItem(pos, 1);
		position->lat = PyFloat_AsDouble(olat);
		position->lon = PyFloat_AsDouble(olon);

		db[i] = position;
	}

	return Py_BuildValue("s", "hola");
}

static PyMethodDef TzposcMaxMethods[] = {
	{"timezone_at", tzpos_timezone_at, METH_VARARGS,
	"Return timezone name from lat and lon."},
	{"load_default_db", tzposc_load_default_db, METH_VARARGS,
	"Load database in memory"},
	{NULL, NULL, 0, NULL},
};

PyMODINIT_FUNC init_tzposc(void)
{
	(void) Py_InitModule("_tzposc", TzposcMaxMethods);
}

int main(int argc, char * argv[])
{
	/*struct Position checkpos;
	char path[1024];
	char * bname;
	time_t tm;
	int i = 0;
	time_t ltm;


	bname = dirname(argv[0]);

	strcat(path, bname);
	strcat(path, "/");
	strcat(path, DB_FILENAME);

	printf("cargando: %s\n", path);

	initdb(path);
	checkpos.lat = 29.095556;
	checkpos.lon = -110.950833;

	tm = time(NULL);
	for (i = 0; i < 7000; i++) 
	{
		(void) timezone_at(checkpos);
	}

	ltm = time(NULL) - tm;

	printf("time spend: %d", ltm);
	
	return 0;*/
	return 0;
}