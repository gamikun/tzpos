﻿import _tzposc
import os
from factory import open_db_file


base_dir = os.path.dirname(os.path.realpath(__file__))
dbname = os.path.join(base_dir, 'tzposdb.data')

_positions = []
_names = []

with open_db_file() as dbfile:
	i = 0
	for line in dbfile:
		parsed = line.split()
		t = [float(parsed[0]), float(parsed[1])]
		_positions.append(t)
		_names.append(parsed[2])

_tzposc.load_default_db(_positions)
timezone_at = _tzposc.timezone_at

if __name__ == '__main__':
	import time

	tm = time.time()

	name = None
	for i in range(1000):
		name = timezone_at(12.123123, 12.4124)

	last_tm = time.time() - tm

	print("timezone: %s" % name)
	print("time spent: %s" % last_tm)