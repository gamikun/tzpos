==========
Change Log
==========

0.1.7 (Sep 7th 2015)
======================
* Added support for continent selection.
  factory.seacher_in_continet

0.1.6 (Aug 24th 2015)
======================
* Fixed an concatenation in timezone function that was causing an error.

0.1.5 (Aug 24th 2015)
======================
* Added missing database file.

0.1.0 (Aug 22th 2015)
======================
* First implementation.
